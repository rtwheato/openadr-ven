# openadr-ven

@CELabs OpenADR VEN implementation for use in projects

code re-used and adapted from https://github.com/MTU-IMES-LAB/OpenADR  
mostly decoupling it from node-red to be used as a standalone VEN in other projects.

to enable console log output set the environmental variable CONSOLE_LOG_VEN=true
or when invoking your app:  CONSOLE_LOG_VEN=true node app.js

to use, create and pass in a config array as so:

```
const adr_ven = require('@celabs/openadr-ven');

const VEN_config = [];
// this should probably be configurable and enabled for multi-VTN      
    VEN_config.vtnName = "my vtn name",
    VEN_config.venName = "my ven name",
    VEN_config.vtnUrl = 123.0.0.1:8080,
    VEN_config.pollRate = 60,
    VEN_config.venID = "abcdef123456789,
    VEN_config.profile = "2.0a;



adr_ven.connect(config, (callback) => {

    console.log('VTN connected');

    console.log('need code to parse/handle this object: ', callback);
    
    return callback;

});
```