'use strict'

const _ = require('lodash'),
  xpath = require('xpath'),
  dom = require('xmldom').DOMParser,
  request = require('request'),
  XMLparser = require('fast-xml-parser');

// to get console.log output, comment out the next line

if (!process.env.CONSOLE_LOG_VEN ){
  console.log = function(d) { };
}

module.exports = exports = {

  connect: (config, callback) => {

    var timerID;

    // HTTP Post Request XML for oadr 2b request registration	
    let myXMLQueryRegistration = `<?xml version="1.0" encoding="utf-8"?> 
    <oadrPayload xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://openadr.org/oadr-2.0b/2012/07">
      <oadrSignedObject>
        <oadrQueryRegistration d3p1:schemaVersion="2.0b" xmlns:d3p1="http://docs.oasis-open.org/ns/energyinterop/201110">
          <requestID xmlns="http://docs.oasis-open.org/ns/energyinterop/201110/payloads">f6fe9f8aa5</requestID>
        </oadrQueryRegistration>
      </oadrSignedObject>
    </oadrPayload>`
    //----------------------------------------------------------------------------------------------------------//
    // HTTP Post Request XML for oadr 2b request registration	
    let myXMLregistration2b = `<?xml version="1.0" encoding="utf-8"?>
    <oadrPayload xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://openadr.org/oadr-2.0b/2012/07">
      <oadrSignedObject>
        <oadrCreatePartyRegistration d3p1:schemaVersion="2.0b" xmlns:d3p1="http://docs.oasis-open.org/ns/energyinterop/201110">
          <requestID xmlns="http://docs.oasis-open.org/ns/energyinterop/201110/payloads">50004</requestID>
          <d3p1:venID>${config.venID}</d3p1:venID>
          <oadrVenName>${config.venName}</oadrVenName>
          <oadrProfileName>${config.profile}</oadrProfileName>
          <oadrTransportName>simpleHttp</oadrTransportName>
          <oadrTransportAddress />
          <oadrReportOnly>false</oadrReportOnly>
          <oadrXmlSignature>false</oadrXmlSignature>
          <oadrHttpPullModel>true</oadrHttpPullModel>
        </oadrCreatePartyRegistration>
      </oadrSignedObject>
    </oadrPayload>`

    request({
      url: config.vtnUrl + '/OpenADR2/Simple/2.0b/EiRegisterParty',
      method: "POST",

      headers: {
        "content-type": "application/xml",  // <--Very important!!!
      },
      body: myXMLQueryRegistration
    }, function (error, response, body) {

      // Error Handling
      if (body == undefined) {
        console.log(response);
        console.log(error);
        oadrQueryRegistration();
      }
      else if (response.statusCode == 500) {
        oadrQueryRegistration();
      }
      else if (response.statusCode == 503) {
        oadrQueryRegistration();
      }
      else {
        console.log("\n VEN ----------oadrQueryRegistration-------------> VTN \n")
        console.log(new Date().toISOString())
        console.log(myXMLQueryRegistration)
        console.log("\n VTN ----------oadrCreatedPartyRegistration------------> VEN \n")
        console.log(new Date().toISOString())
        console.log(body)
        oadrCreatePartyRegistration(config, myXMLregistration2b, callback);
      }
    });
  }
}

function oadrQueryRegistration(config) {

  // HTTP Post Request XML for oadr 2b request registration	
  let myXMLQueryRegistration = `<?xml version="1.0" encoding="utf-8"?>
  <oadrPayload xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://openadr.org/oadr-2.0b/2012/07">
    <oadrSignedObject>
      <oadrQueryRegistration d3p1:schemaVersion="2.0b" xmlns:d3p1="http://docs.oasis-open.org/ns/energyinterop/201110">
        <requestID xmlns="http://docs.oasis-open.org/ns/energyinterop/201110/payloads">f6fe9f8aa5</requestID>
      </oadrQueryRegistration>
    </oadrSignedObject>
  </oadrPayload>`

  request({
    url: config.vtnUrl + '/OpenADR2/Simple/2.0b/EiRegisterParty',
    method: "POST",

    headers: {
      "content-type": "application/xml",  // <--Very important!!!
    },
    body: myXMLQueryRegistration
  }, function (error, response, body) {

    // Error Handling
    if (body == undefined) {
      console.log(response);
      console.log(error);
      oadrQueryRegistration();
    }
    else if (response.statusCode == 500) {
      oadrQueryRegistration();
    }
    else if (response.statusCode == 503) {
      oadrQueryRegistration();
    }
    else {
      console.log("\n VEN ----------oadrQueryRegistration-------------> VTN \n")
      console.log(new Date().toISOString())
      console.log(myXMLQueryRegistration)
      console.log("\n VTN ----------oadrCreatedPartyRegistration------------> VEN \n")
      console.log(new Date().toISOString())
      console.log(body)
      oadrCreatePartyRegistration(myXMLregistration2b);
    }
  });
}

// Function for EiRegistration
function oadrCreatePartyRegistration(config, myXMLregistration2b, callback) {

  var timerID;

  request({
    url: config.vtnUrl + '/OpenADR2/Simple/2.0b/EiRegisterParty',
    method: "POST",

    headers: {
      "content-type": "application/xml",  // <--Very important!!!
    },
    body: myXMLregistration2b
  }, function (error, response, body) {

    console.log("\n VEN ----------oadrCreatePartyRegistration-------------> VTN \n")
    console.log(new Date().toISOString())
    console.log(myXMLregistration2b)
    console.log("\n VTN ----------oadrCreatedPartyRegistration------------> VEN \n")
    console.log(new Date().toISOString())
    console.log(body)
    //return body;
    // Grab data from response xml
    var doc = new dom().parseFromString(body)
    var registrationID = xpath.select("//*[local-name(.)='registrationID']/text()", doc)[0]
    var ven_ID = xpath.select("//*[local-name(.)='venID']/text()", doc)[0]
    console.log("Registration ID is " + registrationID)
    console.log("VEN ID is " + ven_ID)

    // Error Handling
    if (body == undefined) {
      console.log(response);
    }
    else if (response.statusCode == 500) {
      oadrCreatePartyRegistration(config, myXMLregistration2b, callback);
    }
    else if (response.statusCode == 503) {
      oadrCreatePartyRegistration(config, myXMLregistration2b, callback);
    }

    oadrPoll(config, ven_ID, registrationID, callback);

    setTimeout(function () {

      // Something you want delayed.
      requestevent2(config, ven_ID, callback);
    }, 500);

    timerID = setInterval(function () {

      //requestevent2();
      oadrPoll(config, ven_ID, registrationID, callback);

    }, (config.pollRate * 1000 + Math.random()));

  });
}

// Function for EiPoll

function oadrPoll(config, ven_ID, registrationID, callback) {

  // HTTP Post Request XML for oadr 2b Poll
  var myXMLpoll2b = `<?xml version="1.0" encoding="utf-8"?>
  <oadrPayload xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://openadr.org/oadr-2.0b/2012/07">
    <oadrSignedObject>
      <oadrPoll d3p1:schemaVersion="2.0b" xmlns:d3p1="http://docs.oasis-open.org/ns/energyinterop/201110">
        <d3p1:venID>${ven_ID}</d3p1:venID>
      </oadrPoll>
    </oadrSignedObject>
  </oadrPayload>`

  request({
    url: config.vtnUrl + '/OpenADR2/Simple/2.0b/OadrPoll',
    method: "POST",
    headers: {
      "content-type": "application/xml",  // <--Very important!!!
    },
    body: myXMLpoll2b
  }, function (error, response, body) {

    console.log("\n VEN ----oadrPoll-----> VTN \n")
    console.log(new Date().toISOString())
    console.log(myXMLpoll2b)

    // Grab data from response xml
    var doc = new dom().parseFromString(body)
    var requestID = xpath.select("//*[local-name(.)='requestID']/text()", doc)[0]
    var eventID = xpath.select("//*[local-name(.)='eventID']/text()", doc)[0]
    var modificationNumber = xpath.select("//*[local-name(.)='modificationNumber']/text()", doc)[0]

    //HTTP Post Request XML for createdEvent
    var myXMLcreatedevent = `<?xml version="1.0" encoding="utf-8"?>
    <oadrPayload xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://openadr.org/oadr-2.0b/2012/07">
      <oadrSignedObject>
        <oadrCreatedEvent d3p1:schemaVersion="2.0b" xmlns:d3p1="http://docs.oasis-open.org/ns/energyinterop/201110">
          <eiCreatedEvent xmlns="http://docs.oasis-open.org/ns/energyinterop/201110/payloads">
            <d3p1:eiResponse>
              <d3p1:responseCode>200</d3p1:responseCode>
              <d3p1:responseDescription>OK</d3p1:responseDescription>
              <requestID />
            </d3p1:eiResponse>
            <d3p1:eventResponses>
              <d3p1:eventResponse>
                <d3p1:responseCode>200</d3p1:responseCode>
                <d3p1:responseDescription>OK</d3p1:responseDescription>
                <requestID>${requestID}</requestID>
                <d3p1:qualifiedEventID>
                  <d3p1:eventID>${eventID}</d3p1:eventID>
                  <d3p1:modificationNumber>${modificationNumber}</d3p1:modificationNumber>
                </d3p1:qualifiedEventID>
                <d3p1:optType>optIn</d3p1:optType>
              </d3p1:eventResponse>
            </d3p1:eventResponses>
            <d3p1:venID>${ven_ID}</d3p1:venID>
          </eiCreatedEvent>
        </oadrCreatedEvent>
      </oadrSignedObject>
    </oadrPayload>`

    if (body == undefined) {
      //console.log(response);
      oadrPoll(config, ven_ID, registrationID, callback);
    }
    else if (body.indexOf("oadrDistributeEvent") > -1) {
      //var msg = { payload:body }
      //node.send(msg);
      oadrDistributeEvent(body, callback);
      if (body.indexOf("oadrResponseRequired>always<") > -1) {
        oadrCreatedEvent(config, myXMLcreatedevent);
      }

      var array_value = [];
      var array_signalID = [];
      for (var i = 0; i < 50; i++) {
        var signalID = xpath.select("//*[local-name(.)='signalID']/text()", doc)[i]
        var value = xpath.select("//*[local-name(.)='currentValue']/*[local-name(.)='payloadFloat']/*[local-name(.)='value']/text()", doc)[i]
        array_value.push('"' + value + '"')
        array_signalID.push('"' + signalID + '"')
        if (value === undefined) { break; }

        else if (value !== undefined) {

        }
      }

      var msg = { payload: body, all_data: '{"signalID":[' + array_signalID + '], "CurrentValue":[' + array_value + ']}', value: array_value }
      //node.send(msg);
      // do something with the event
      oadrDistributeEvent(msg, callback);

      console.log("\n VTN ----oadrDistributeEvent-----> VEN \n")
      console.log(new Date().toISOString())
      console.log(body)
    }

    else if (body.indexOf("oadrResponse") > -1) {
      console.log("\n VTN ----oadrResponse-----> VEN \n")
      console.log(new Date().toISOString())
      console.log(body)
      //console.log(body)
    }

    else if (body.indexOf("oadrRequestRegistration") > -1) {
      console.log("\n VTN-------oadrRequestRegistration---------->  VEN \n")
      console.log(new Date().toISOString())
      console.log(body)
      oadrCreatePartyRegistration(config, myXMLregistration2b, callback);
    }

    else if (body.indexOf("oadrCreateReport") > -1) {
      console.log(body)
    }

    else if (body.indexOf("oadrRegisterReport") > -1) {
      console.log("\n VTN ----oadrRegisterReport-----> VEN \n")
      console.log(body)

      oadrRegisteredReport(config, ven_ID);
    }

    else if (body.indexOf("oadrCancelReport") > -1) {
      console.log(body)
    }

    else if (body.indexOf("oadrUpdateReport") > -1) {
      console.log(body)
    }

    else if (body.indexOf("oadrCancelPartyRegistration") > -1) {
      console.log("\n VTN ------------oadrCancelPartyRegistration--------------> VEN \n")
      console.log(new Date().toISOString())
      console.log(body)
      console.log("\n VEN ------------oadrCanceledPartyRegistration--------------> VTN \n")
      console.log(new Date().toISOString())
      var myXMLCanceledRegistration = `<?xml version="1.0" encoding="utf-8"?>
      <oadrPayload xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://openadr.org/oadr-2.0b/2012/07">
        <oadrSignedObject>
          <oadrCanceledPartyRegistration d3p1:schemaVersion="2.0b" xmlns:d3p1="http://docs.oasis-open.org/ns/energyinterop/201110">
            <d3p1:eiResponse>
              <d3p1:responseCode>200</d3p1:responseCode>
              <requestID xmlns="http://docs.oasis-open.org/ns/energyinterop/201110/payloads">9b9752581e7c18fe5268</requestID>
            </d3p1:eiResponse>
            <d3p1:registrationID>${registrationID}</d3p1:registrationID>
            <d3p1:venID>${ven_ID}</d3p1:venID>
          </oadrCanceledPartyRegistration>
        </oadrSignedObject>
      </oadrPayload>`
      console.log(myXMLCanceledRegistration)

      oadrCreatePartyRegistration(config, myXMLCanceledRegistration);
    }

    else if (body.indexOf("oadrRequestReregistration") > -1) {
      console.log("\n VTN ------------oadrRequestReregistration--------------> VEN \n")
      console.log(new Date().toISOString())
      console.log(body)
      oadrResponse(config, ven_ID);

      // HTTP Post Request XML for oadr 2b request registration	
      var myXMLReregistration = `<?xml version="1.0" encoding="utf-8"?>
      <oadrPayload xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://openadr.org/oadr-2.0b/2012/07">
        <oadrSignedObject>
          <oadrCreatePartyRegistration d3p1:schemaVersion="2.0b" xmlns:d3p1="http://docs.oasis-open.org/ns/energyinterop/201110">
            <requestID xmlns="http://docs.oasis-open.org/ns/energyinterop/201110/payloads">50004</requestID>
            <d3p1:registrationID>${registrationID}</d3p1:registrationID>
            <d3p1:venID>${config.venID}</d3p1:venID>
            <oadrVenName>${config.venName}</oadrVenName>
            <oadrProfileName>${config.profile}</oadrProfileName>
            <oadrTransportName>simpleHttp</oadrTransportName>
            <oadrTransportAddress />
            <oadrReportOnly>false</oadrReportOnly>
            <oadrXmlSignature>false</oadrXmlSignature>
            <oadrHttpPullModel>true</oadrHttpPullModel>
          </oadrCreatePartyRegistration>
        </oadrSignedObject>
      </oadrPayload>`
      oadrCreatePartyRegistration(config, myXMLReregistration, callback);
    }

    //console.log(body)
    // Error Handling

    else if (response.statusCode == 500) {
      oadrPoll(config, ven_ID, registrationID, callback);
    }

  });

}

function oadrRegisteredReport(config, ven_ID) {
  // Post Request XML for oadr 2b Registered Report
  var myXMLregisteredreport = `<?xml version="1.0" encoding="utf-8"?> 
  <oadrPayload xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://openadr.org/oadr-2.0b/2012/07">   
    <oadrSignedObject>     
      <oadrRegisteredReport d3p1:schemaVersion="2.0b" xmlns:d3p1="http://docs.oasis-open.org/ns/energyinterop/201110">
        <d3p1:eiResponse>
          <d3p1:responseCode>200</d3p1:responseCode>
          <d3p1:responseDescription>OK</d3p1:responseDescription>
          <requestID xmlns="http://docs.oasis-open.org/ns/energyinterop/201110/payloads">1e7f5f377168380276ab</requestID>
        </d3p1:eiResponse>
        <d3p1:venID>${ven_ID}</d3p1:venID>     
      </oadrRegisteredReport>
    </oadrSignedObject>
  </oadrPayload>`

  request({
    url: config.vtnUrl + '/OpenADR2/Simple/2.0b/EiReport',
    method: "POST",
    headers: {
      "content-type": "application/xml",  // <--Very important!!!
    },
    body: myXMLregisteredreport
  }, function (error, response, body) {
    console.log("\n VEN ----oadrRegisteredReport-----> VTN \n")
    console.log(new Date().toISOString())
    console.log(myXMLregisteredreport)
    console.log("\n VTN ----oadrResponse-----> VEN \n")
    console.log(new Date().toISOString())
    console.log(body)
    // Error Handling
    if (body == undefined) {
      console.log(response);
      oadrRegisteredReport(config, ven_ID);
    }
    else if (response.statusCode == 500) {
      oadrRegisteredReport(config, ven_ID);
    }
    else if (response.statusCode == 503) {
      oadrRegisteredReport(config, ven_ID);
    }
  });
}


//----------------------------------------------------------------------------------------------------------------//

function oadrResponse(config, ven_ID) {

  var myXMLresponse = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
  <ns2:oadrPayload xmlns:ns1="http://docs.oasis-open.org/ns/energyinterop/201110" 
      xmlns:ns2="http://openadr.org/oadr-2.0b/2012/07" 
      xmlns:ns3="http://www.w3.org/2005/Atom" 
      xmlns:ns4="http://docs.oasis-open.org/ns/emix/2011/06/power" 
      xmlns:ns5="http://docs.oasis-open.org/ns/emix/2011/06/siscale" 
      xmlns:ns6="http://www.opengis.net/gml/3.2" 
      xmlns:ns7="http://www.w3.org/2009/xmldsig11#" 
      xmlns:ns8="http://www.w3.org/2000/09/xmldsig#" 
      xmlns:ns9="http://openadr.org/oadr-2.0b/2012/07/xmldsig-properties" 
      xmlns:ns10="urn:ietf:params:xml:ns:icalendar-2.0" 
      xmlns:ns11="http://docs.oasis-open.org/ns/emix/2011/06" 
      xmlns:ns12="http://docs.oasis-open.org/ns/energyinterop/201110/payloads" 
      xmlns:ns13="urn:ietf:params:xml:ns:icalendar-2.0:stream" 
      xmlns:ns14="urn:un:unece:uncefact:codelist:standard:5:ISO42173A:2010-04-07">
    <ns2:oadrSignedObject>
      <ns2:oadrResponse ns1:schemaVersion="2.0b">
        <ns1:eiResponse>
          <ns1:responseCode>200</ns1:responseCode>
          <ns1:responseDescription>OK</ns1:responseDescription>
          <ns12:requestID></ns12:requestID>
        </ns1:eiResponse>
        <ns1:venID>${ven_ID}</ns1:venID>
      </ns2:oadrResponse>
    </ns2:oadrSignedObject>
  </ns2:oadrPayload>`


  request({
    url: config.vtnUrl + '/OpenADR2/Simple/2.0b/EiReport',
    method: "POST",
    headers: {
      "content-type": "application/xml",  // <--Very important!!!
    },
    body: myXMLresponse
  }, function (error, response, body) {
    console.log("\n VEN ----oadrResponse-----> VTN \n")
    console.log(new Date().toISOString())
    console.log(myXMLresponse)
    console.log("\n VTN ----http(200)-----> VEN \n")
    console.log(new Date().toISOString())
    console.log(body)
    // Error Handling
    if (body == undefined) {
      console.log(response);
      oadrResponse(config, ven_ID);
    }
    else if (response.statusCode == 500) {
      oadrResponse(config, ven_ID);
    }
    else if (response.statusCode == 503) {
      oadrResponse(config, ven_ID);
    }

  });
}
//-------------------------------------------------------------------------------------------------------------------------//

// Function for EiRequestEvent 2.0b
function requestevent2(config, ven_ID, callback) {

  // HTTP Post Request XML for oadr 2b Request Event
  var myXMLrequesteventb = `<?xml version="1.0" encoding="utf-8"?>
  <oadrPayload xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://openadr.org/oadr-2.0b/2012/07">
    <oadrSignedObject>
      <oadrRequestEvent d3p1:schemaVersion="2.0b" xmlns:d3p1="http://docs.oasis-open.org/ns/energyinterop/201110">
        <eiRequestEvent xmlns="http://docs.oasis-open.org/ns/energyinterop/201110/payloads">
          <requestID></requestID>
          <d3p1:venID>${ven_ID}</d3p1:venID> 
        </eiRequestEvent>
      </oadrRequestEvent>
    </oadrSignedObject>
  </oadrPayload>`

  request({
    url: config.vtnUrl + '/OpenADR2/Simple/2.0b/EiEvent',
    method: "POST",
    headers: {
      "content-type": "application/xml",  // <--Very important!!!
    },
    body: myXMLrequesteventb
  }, function (error, response, body) {
    console.log("\n VEN ----oadrRequestEvent-----> VTN \n")
    console.log(new Date().toISOString())
    console.log(myXMLrequesteventb)

    // Grab data from response xml

    var doc = new dom().parseFromString(body)
    var requestID = xpath.select("//*[local-name(.)='requestID']/text()", doc)[0]
    var eventID = xpath.select("//*[local-name(.)='eventID']/text()", doc)[0]
    var modificationNumber = xpath.select("//*[local-name(.)='modificationNumber']/text()", doc)[0]

    // HTTP Post Request XML for createdEvent
    var myXMLcreatedevent = `<?xml version="1.0" encoding="utf-8"?> <oadrPayload xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://openadr.org/oadr-2.0b/2012/07">
    <oadrSignedObject>
      <oadrCreatedEvent d3p1:schemaVersion="2.0b" xmlns:d3p1="http://docs.oasis-open.org/ns/energyinterop/201110">
        <eiCreatedEvent xmlns="http://docs.oasis-open.org/ns/energyinterop/201110/payloads">
          <d3p1:eiResponse>
            <d3p1:responseCode>200</d3p1:responseCode>
            <d3p1:responseDescription>OK</d3p1:responseDescription>
            <requestID />
          </d3p1:eiResponse>
          <d3p1:eventResponses>
            <d3p1:eventResponse>
              <d3p1:responseCode>200</d3p1:responseCode>
              <d3p1:responseDescription>OK</d3p1:responseDescription>
              <requestID>${requestID}</requestID>
              <d3p1:qualifiedEventID>
                <d3p1:eventID>${eventID}</d3p1:eventID>
                <d3p1:modificationNumber>${modificationNumber}</d3p1:modificationNumber>
              </d3p1:qualifiedEventID>
              <d3p1:optType>optIn</d3p1:optType>
            </d3p1:eventResponse>
          </d3p1:eventResponses>
          <d3p1:venID>${ven_ID}</d3p1:venID>
        </eiCreatedEvent>
      </oadrCreatedEvent>
    </oadrSignedObject>
  </oadrPayload>`
    console.log("\n VTN ----myXMLcreatedevent-----> VEN \n")
    console.log(new Date().toISOString())
    console.log(myXMLcreatedevent)

    // Error Handling
    if (body == undefined) {
      console.log(response);
      requestevent2(config, ven_ID, callback);
    }

    else if (body.indexOf("oadrDistributeEvent") > -1) {
      //var msg = { payload:body }
      //node.send(msg);
      oadrDistributeEvent(body, callback);

      if (body.indexOf("oadrResponseRequired>always<") > -1) {
        oadrCreatedEvent(config, myXMLcreatedevent);
      }

      var array_signalID = [];
      var array_value = [];
      for (var i = 0; i < 50; i++) {

        var signalID = xpath.select("//*[local-name(.)='eiEvent']/*[local-name(.)='eiEventSignals']/*[local-name(.)='eiEventSignal']/*[local-name(.)='signalID']/text()", doc)[i]
        var value = xpath.select("//*[local-name(.)='eiEvent']/*[local-name(.)='eiEventSignals']/*[local-name(.)='eiEventSignal']/*[local-name(.)='currentValue']/*[local-name(.)='payloadFloat']/*[local-name(.)='value']/text()", doc)[i]
        array_signalID.push('"' + signalID + '"');
        array_value.push('"' + value + '"');

        if (value !== undefined) {


        }
        if (value === undefined) { break; }
      }
      var msg = { payload: body, all_data: '{"signalID":[' + array_signalID + '], "CurrentValue":[' + array_value + ']}', value: array_value }
      console.log(msg);

      console.log("\n VTN ----oadrDistributeEvent-----> VEN \n")
      console.log(new Date().toISOString())
      console.log(body)

      //console.log(body)
    }

    else if (body.indexOf("oadrResponse") > -1) {
      console.log("\n VTN ----oadrResponse-----> VEN \n")
      console.log(new Date().toISOString())
      console.log(body)
    }
    else if (response.statusCode == 500) {
      requestevent2(config, ven_ID, callback);
    }
    else if (response.statusCode == 503) {
      requestevent2(config, ven_ID, callback);
    }

  });
}
//-----------------------------------------------------------------------------------------------------------------//

function oadrCreatedEvent(config, myXMLcreatedevent) {

  request({
    url: config.vtnUrl + '/OpenADR2/Simple/2.0b/EiEvent',
    method: "POST",
    headers: {
      "content-type": "application/xml",  // <--Very important!!!
    },
    body: myXMLcreatedevent
  }, function (error, response, body) {

    console.log("\n VEN ----oadrCreatedEvent------> VTN \n")
    console.log(new Date().toISOString())
    console.log(myXMLcreatedevent)
    console.log("\n VTN ----oadrResponse------> VEN \n")
    console.log(new Date().toISOString())
    console.log(body)
    // Error Handling
    if (body == undefined) {
      console.log(response);
      oadrCreatedEvent(config, myXMLcreatedevent);
    }
    else if (response.statusCode == 500) {
      oadrCreatedEvent(config, myXMLcreatedevent);
    }
    else if (response.statusCode == 503) {
      oadrCreatedEvent(config, myXMLcreatedevent);
    }
  });
}

//-------------------------------------------------------------------------------------------------------------------//

function oadrRegisteredReport(config, ven_ID) {

  // Post Request XML for oadr 2b Registered Report
  var myXMLregisteredreport = `<?xml version="1.0" encoding="utf-8"?>
  <oadrPayload xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://openadr.org/oadr-2.0b/2012/07">
    <oadrSignedObject>
      <oadrRegisteredReport d3p1:schemaVersion="2.0b" xmlns:d3p1="http://docs.oasis-open.org/ns/energyinterop/201110">
        <d3p1:eiResponse>
          <d3p1:responseCode>200</d3p1:responseCode>
          <d3p1:responseDescription>OK</d3p1:responseDescription>
          <requestID xmlns="http://docs.oasis-open.org/ns/energyinterop/201110/payloads">1e7f5f377168380276ab</requestID>
        </d3p1:eiResponse>
        <d3p1:venID>${ven_ID}</d3p1:venID>
      </oadrRegisteredReport>
    </oadrSignedObject>
  </oadrPayload>`

  request({
    url: config.vtnUrl + '/OpenADR2/Simple/2.0b/EiReport',
    method: "POST",
    headers: {
      "content-type": "application/xml",  // <--Very important!!!
    },
    body: myXMLregisteredreport
  }, function (error, response, body) {
    console.log("\n VEN ----oadrRegisteredReport-----> VTN \n")
    console.log(new Date().toISOString())
    console.log(myXMLregisteredreport)
    console.log("\n VTN ----oadrResponse-----> VEN \n")
    console.log(new Date().toISOString())
    console.log(body)
    // Error Handling
    if (body == undefined) {
      console.log(response);
      oadrRegisteredReport(config, ven_ID);
    }
    else if (response.statusCode == 500) {
      oadrRegisteredReport(config, ven_ID);
    }
    else if (response.statusCode == 503) {
      oadrRegisteredReport(config, ven_ID);
    }

  });
}

function oadrDistributeEvent(data, callback) {

  var options = {
    ignoreNameSpace : true
  };

  if (XMLparser.validate(data) === true) {

    var xml2json = XMLparser.parse(data, options);
    console.log('\n VEN ---- converted payload to return to callback ----> \n');
    console.log(xml2json);
    console.log('\n API <---- converted payload to return to callback ---- \n');

    callback(xml2json);


  }

}
